<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage prisantya
 * @since 1.0
 */

add_action( 'after_setup_theme', 'prisantya_setup' );
function prisantya_setup() {

	register_nav_menus( array(
		'top'    => __( 'Header Menu', 'prisantya' ),
		'secondary' => __( 'Footer Menu', 'prisantya' ),
	) );
	
	add_theme_support('title-tag');

	add_theme_support( 'post-thumbnails' );
}

function gby_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'prisantya' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'prisantya' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gby_widgets_init' );

function gby_adding_bootstrap() {
	wp_enqueue_script('jquery');
	wp_register_style('bootstrap',  get_template_directory_uri().'/vendor/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style('bootstrap');
	wp_register_style('blogspot',  get_template_directory_uri().'/css/blog-post.css');
	wp_enqueue_style('blogspot');
	wp_register_style('heroic',  get_template_directory_uri().'/css/heroic-features.css');
	wp_enqueue_style('heroic');
	wp_register_script('script',  get_template_directory_uri().'/vendor/bootstrap/js/bootstrap.bundle.min.js');
	wp_enqueue_script('script');
	}
add_action( 'wp_enqueue_scripts', 'gby_adding_bootstrap' );  

