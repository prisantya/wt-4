<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage prisantya
 * @since Twenty Nineteen 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */

?>  
  
 

  <!-- Post Content Column -->
  <div class="col-lg-8">

    <!-- Title -->
    <h1 class="mt-4"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

    <!-- Author -->
    <p class="lead">
      by
      <?php the_author(); ?>
    </p>

    <hr>

    <!-- Date/Time -->
    <p>Posted on <?php the_date(); ?></p>

    <hr>

    <hr>

    <!-- Post Content -->
    <?php the_content(); ?>
    <hr>
  </div>



